set -ex

# docker-hub username
USER_NAME=dmmop
# Image name
IMAGE=calibre-importer

docker build -t ${USER_NAME}/${IMAGE}:latest .

# ensure we're up to date
git pull
# bump version
version=`cat VERSION`
echo "version: ${version}"

# tag it
git add -A
git commit -m "version ${version}"
git tag -a "${version}" -m "version ${version}"
git push
git push --tags
docker tag "${USER_NAME}/${IMAGE}:latest" ${USER_NAME}/${IMAGE}:${version}
# push it
docker push "${USER_NAME}/${IMAGE}:latest"
docker push "${USER_NAME}/${IMAGE}:${version}"
