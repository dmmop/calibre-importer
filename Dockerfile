# Use an Alpine linux base image with GNU libc (aka glibc) pre-installed, courtesy of Vlad Frolov

FROM frolvlad/alpine-glibc

MAINTAINER dmmop

#########################################
##        ENVIRONMENTAL CONFIG         ##
#########################################
# Calibre environment variables
ENV CALIBRE_LIBRARY_DIRECTORY=/opt/calibredb/library

# Auto-import directory
ENV CALIBRE_IMPORT_DIRECTORY=/opt/calibredb/import

# Outputs extensions
ENV CALIBRE_OUTPUT_EXTENSIONS="mobi epub"

# File watcher delay (s)econd, (m)inutes, (h)ours, (d)ays
ENV DELAY_TIME="1m"

# Flag for automatically updating to the latest version on startup
ENV AUTO_UPDATE=0



#########################################
##     CONFIG USER FOR EXECUTION       ##
#########################################
# Set user permisions
ENV PUID=1000
ENV PGID=1000
ENV SERVICE_NAME=calibre

RUN addgroup -g ${PGID} ${SERVICE_NAME} && \
	adduser -u ${PUID} --disabled-password -H -D -S -g ${PGID} ${SERVICE_NAME}


#########################################
##         DEPENDENCY INSTALL          ##
#########################################
RUN apk update && \
    apk add --no-cache --upgrade \
    bash \
    ca-certificates \
    python3 \
    wget \
    gcc \
    mesa-gl \
    imagemagick \
    qt5-qtbase-x11 \
    xdg-utils \
    jq \
    xz && \
#########################################
##            APP INSTALL              ##
#########################################
    wget -O- https://raw.githubusercontent.com/kovidgoyal/calibre/master/setup/linux-installer.py | python3 -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main(install_dir='/opt', isolated=True)" && \
    rm -rf /tmp/calibre-installer-cache





#########################################
##            Script Setup             ##
#########################################
COPY auto_import.sh /opt/auto_import.sh
COPY add_formats.sh /opt/add_formats.sh
COPY VERSION VERSION
RUN chmod a+x /opt/auto_import.sh
RUN chmod a+x /opt/add_formats.sh

#########################################
##         EXPORTS AND VOLUMES         ##
#########################################
VOLUME /opt/calibredb/import
VOLUME /opt/calibredb/library

# Set user for scripts execution
#USER ${SERVICE_NAME}

#########################################
##           Startup Command           ##
#########################################
CMD ["/bin/bash", "/opt/auto_import.sh"]

